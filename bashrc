# Add npm modules to $PATH
export PATH="$HOME/.npm/bin:$PATH"

# Add cms modules to $PYTHONPATH
export PYTHONPATH="$HOME/.gitlab/cms:$HOME/.gitlab/sitescripts:$PYTHONPATH"